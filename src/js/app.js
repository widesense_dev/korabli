import 'simplebar';
import $ from 'jquery';
import 'selectric';
import 'simplebar';
import '@fancyapps/fancybox';

window.$ = $;

import './lib/inputmaks';

import './components/main';
import './components/jquery.sTabs';
import './components/remcalc.js';
import debounce from './components/debounce';

import './sections/contacts';
import './sections/progress';
import './sections/gallery';
import './sections/news';
import './sections/plans';
import './sections/calculator';

import {ACTIVE} from './_const';

$('.tabs').sTabs();

$(window).on('load', function() {
  setTimeout(() => {
    $('body').addClass('is-ready');
  }, 300);
});

$('.btn-burger').on('click', function(e) {
  e.preventDefault();

  if ($(this).hasClass(ACTIVE)) {
    $.fn.fullpage.setAllowScrolling(true);
  } else {
    $.fn.fullpage.setAllowScrolling(false);
  }

  $(this).toggleClass(ACTIVE);
  $('.menuMobile').toggleClass(ACTIVE);
  $('body').toggleClass('show-menu');
});

$('.menuMobile .nav a').on('click', function() {
  $.fn.fullpage.setAllowScrolling(true);
  $.fn.fullpage.setAllowScrolling(false);
  $('.btn-burger').removeClass(ACTIVE);
  $('.menuMobile').removeClass(ACTIVE);
  $('body').removeClass('show-menu');
});

$('.js-select').selectric();

$('[name="phone"]').mask('+7 (999) 999-99-99');

// if ($(window).width() > 480) {
//   $('body').append(` <div class="tooltip">
//             <div class="tooltip__inner"></div>
//         </div>`);
//
//
//   var tooltipSpan = document.querySelector('.tooltip');
//
//   window.onmousemove = function(e) {
//     var x = e.clientX,
//       y = e.clientY;
//     tooltipSpan.style.top = y - 50 + 'px';
//
//     if (x > 1000) {
//       tooltipSpan.style.left = (x - 400) / 16 + 'rem';
//
//       $('.tooltip').addClass('is-reverse');
//     } else {
//       tooltipSpan.style.left = (x + 30) / 16 + 'rem';
//       $('.tooltip').removeClass('is-reverse');
//     }
//   };
//
//   $(document).on({
//     mouseenter: function() {
//       //stuff to do on mouse enter
//
//       if ($(this).data('apartment')) $('.tooltip').addClass('is-orange');
//       $('.tooltip').addClass(ACTIVE);
//     },
//     mouseleave: function() {
//       //stuff to do on mouse leave
//       $('.tooltip').removeClass(ACTIVE);
//     }
//   }, '[data-canvas-floor], [data-apartment]');
//
//   $(document).on({
//     mouseenter: debounce(function() {
//       //stuff to do on mouse enter
//
//       let self = $(this);
//       let data;
//       let url;
//
//       if (self.data('canvas-floor')) {
//         data = self.data('canvas-floor');
//         url = $('[name="find-floor-info"]').attr('content');
//       }
//
//       if (self.data('apartment')) {
//         data = self.data('data-apartment');
//         url = $('[name="find-apartment-info"]').attr('content');
//       }
//
//       console.log(url);
//
//       $.ajax({
//         method: 'GET',
//         url: url,
//         dataType: 'html',
//         data: {
//           id: data
//         },
//         beforeSend: function() {
//           $('.tooltip').addClass('is-loading');
//         },
//         success: function(data) {
//           $('.tooltip__inner').replaceWith(data);
//           $('.tooltip').removeClass('is-loading');
//         }
//       });
//     }, 200)
//   }, '[data-canvas-floor], [data-apartment]');
// }

