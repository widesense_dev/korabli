// require('@fancyapps/fancybox');

// import '../lib/scrolloverflow';

import IScroll from '../lib/iscroll';
import fullpage from 'fullpage.js';
import svg4everybody from 'svg4everybody';
import '../components/togglepopup';
import $ from 'jquery';
import {ACTIVE} from '../_const';
import {roomSlider} from '../sections/plans';


// new fullpage('.page-content', {
//   verticalCentered: false,
//   anchors: ['banner', 'about', 'location', 'finance', 'progress', 'contacts'],
//   licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
//   menu: '.menu',
//   navigation: true,
//   navigationPosition: 'right',
//   fitToSection: false,
//   css3: true,
//   scrollOverflow: true,
//   // afterLoad: function afterLoad() {
//   //   $('body').removeClass('is-single-show');
//   // }
// });

$('.page-content').fullpage({
  verticalCentered: false,
  anchors: ['banner', 'about', 'location','layout', 'finance', 'progress', 'contacts'],
  licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
  menu: '.menu',
  navigation: true,
  navigationPosition: 'right',
  fitToSection: false,
  css3: true,
  afterRender: function() {
    $('.page-content').addClass('is-loaded');
    initParams();
  },
  responsive: 479
});

// if ($(window).width() > 767) {
//   $('.page-content').fullpage({
//     verticalCentered: false,
//     anchors: ['banner', 'about', 'location','layout', 'finance', 'progress', 'contacts'],
//     licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
//     menu: '.menu',
//     navigation: true,
//     navigationPosition: 'right',
//     fitToSection: false,
//     css3: true,
//     afterRender: function() {
//       $('.page-content').addClass('is-loaded');
//     }
//     // scrollOverflow: true,
//     // afterLoad: function afterLoad() {
//     //   $('body').removeClass('is-single-show');
//     // }
//   });
// } else {
// $('.page-content').fullpage({
//   verticalCentered: false,
//   anchors: ['banner', 'about', 'location','layout', 'finance', 'progress', 'contacts'],
//   licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
//   menu: '.menu',
//   navigation: true,
//   navigationPosition: 'right',
//   fitToSection: false,
//   css3: true,
//   // responsive: 480,
//   scrollOverflow: true,
//   // afterLoad: function afterLoad() {
//   //   $('body').removeClass('is-single-show');
//   // }
// });
// }

svg4everybody();

$(document).on('click', '.btn-scrollDown', function() {
  $.fn.fullpage.moveSectionDown();
});


// $('[data-popup]').on('click', function(event) {
//   event.preventDefault();
//   let idPopup = $(this).attr('data-popup');
//   $(idPopup).addClass('is-active');
//   $('body').addClass('no-scroll');
//   $.fn.fullpage.setAllowScrolling(false);
// });
//
// $('.modal').on('click', function(event) {
//   let self = $(this);
//   if ($(event.target).is('.modal-close') || $(event.target).is('.modal-container')) {
//     event.preventDefault();
//     self.removeClass('is-active');
//     $('body').removeClass('no-scroll');
//     $.fn.fullpage.setAllowScrolling(true);
//   }
// });


$('.menu__item').on('click', function() {
  $.fn.fullpage.setAllowScrolling(true);

  $('.modal').removeClass('is-active');
  $('.popup').removeClass('is-active');
  $('body').removeClass('no-scroll');
});


function initParams() {
  function getQueryParam(url, key) {
    var queryStartPos = url.indexOf('?');
    if (queryStartPos === -1) {
      return;
    }
    var params = url.substring(queryStartPos + 1).split('&');
    for (var i = 0; i < params.length; i++) {
      var pairs = params[i].split('=');
      if (decodeURIComponent(pairs.shift()) === key) {
        return decodeURIComponent(pairs.join('='));
      }
    }
  }

  let modalCurrent = getQueryParam(window.location.search, 'modal');

  if ($(`#${modalCurrent}`).length) {

    let idPopup = '#' + modalCurrent;

    if ($(window).width() > 479) {
      $('.popup').removeClass(ACTIVE);
      $(idPopup).addClass(ACTIVE);
    } else {

      if (idPopup === '#canvasObject') {
        idPopup = '#selectionNumber';
      }

      $.fancybox.open({
        src: idPopup,
        type: 'inline',
        opts: {
          afterShow: function(instance, current) {
            console.info('done!');
          }
        }
      });

    }

    // document.location.search = $.param({modal: idPopup.slice(1)});

    window.history.pushState({}, '', window.location.origin + '/');

    window.history.pushState({}, '', window.location.href + '?' + $.param({modal: idPopup.slice(1)}));

    $('body').addClass('no-scroll');


    setTimeout(() => {
      $.fn.fullpage.setAllowScrolling(false);
      $.fn.fullpage.moveTo($(idPopup).closest('.section').data('anchor'));
    }, 300);

  }

  let roomCurrent = getQueryParam(window.location.search, 'roomId');

  if (roomCurrent !== undefined) {

    let idRoom = roomCurrent;
    let urlAppart = $('[name="find-apartment"]').attr('content');

    if ($(window).width() > 479) {

      $('.popup').removeClass(ACTIVE);
      $('#roomCard').addClass(ACTIVE).addClass('is-loading');

    } else {
      $.fancybox.open({
        src: '#roomCard',
        type: 'inline',
        opts: {
          afterShow: function(instance, current) {
            console.info('done!');
          }
        }
      });
    }

    $('body').addClass('no-scroll');
    $.fn.fullpage.setAllowScrolling(false);

    $.ajax({
      method: 'POST',
      dataType: 'json',
      url: urlAppart,
      data: {
        id: idRoom
      },
      success: function(data) {

        window.history.pushState({}, '', window.location.origin + '/');

        window.history.pushState({}, '', window.location.href + '?' + $.param({roomId: idRoom}));

        if (data === null) {
          alert('������ ������� NULL');

          $('.popup').removeClass(ACTIVE);
          $('#objectRooms').addClass(ACTIVE);
          $('#roomCard').removeClass('is-loading');
          $('body').addClass('no-scroll');
          $.fn.fullpage.setAllowScrolling(false);

          return false;
        }


        $('#roomCard .roomCard__title').text(`${data.title}`);
        $('#roomCard .roomCard__square').text(`${data.square}`);
        $('#roomCard .roomCard__floor').text(`${data.floor}`);
        $('#roomCard .roomCard__text').text(`${data.description}`);
        $('#roomCard .priceBox__value').text(`${data.price}`);
        $('#roomCard .previewRoom .swiper-wrapper').html('');

        $('#roomCard .profitBox__body').html(data.profit);

        data.images.forEach((item) => {
          let templateSlide = `
          <div class="swiper-slide">
            <div class="previewRoom__photo" style="background-image: url(${item});"></div>
          </div>
      `;
          $('#roomCard .previewRoom .swiper-wrapper').append(templateSlide);

          roomSlider.init();

          $('#roomCard').removeClass('is-loading');
        });
      }
    });

  }
}
