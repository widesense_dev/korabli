import {ACTIVE} from '../_const';
import $ from 'jquery';
import {roomSlider} from '../sections/plans';

$('[data-fancybox]').fancybox();

$('[data-popup]').on('click', function(event) {
  event.preventDefault();
  let idPopup = $(this).attr('data-popup');

  if ($(window).width() > 479) {
    $('.popup').removeClass(ACTIVE);
    $(idPopup).addClass(ACTIVE);
  } else {

    if (idPopup === '#canvasObject') {
      idPopup = '#selectionNumber';
    }

    $.fancybox.open({
      src: idPopup,
      type: 'inline',
      opts: {
        afterShow: function(instance, current) {
          console.info('done!');
        }
      }
    });

  }

  // document.location.search = $.param({modal: idPopup.slice(1)});

  window.history.pushState({}, '', '/');

  window.history.pushState({}, '', window.location.href + '?' + $.param({modal: idPopup.slice(1)}));

  $('body').addClass('no-scroll');
  $.fn.fullpage.setAllowScrolling(false);

  // history.pushState('', '', `${window.location.hash}/${idPopup}`);
});

$('.modal').on('click', function(event) {
  let self = $(this);

  if ($(event.target).is('.modal-close') || $(event.target).is('.modal-container')) {

    event.preventDefault();
    self.removeClass(ACTIVE);
    $('body').removeClass('no-scroll');
    $.fn.fullpage.setAllowScrolling(true);
  }
});

$('.btn-close').on('click', function(e) {

  if ($(this).closest('#roomCard').length) {
    $('.popup').removeClass(ACTIVE);

    if (window.navWatch === '') {
      $('#objectRooms').addClass('is-active');
    } else {
      $('#selectionNumber').addClass('is-active');
    }

    return false;
  }

  if ($(this).closest('#objectRooms').length) {
    $('.popup').removeClass(ACTIVE);
    $('#canvasObject').addClass(ACTIVE);
    return false;
  }

  if ($(window).width() > 479) {
    $('.popup').removeClass(ACTIVE);
  } else {
    $.fancybox.close();
  }

  window.history.pushState({}, '', '/');

  $('body').removeClass('no-scroll');
  $.fn.fullpage.setAllowScrolling(true);

  e.preventDefault();
});
