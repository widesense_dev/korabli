import initBox from './initBox';
import {styles as StyleMap} from './style';

export default function contactsMap() {

  let MarkerWithLabel = require('./markerwithlabel.js')(google.maps);
  let newMarkers = [];
  let {zoom, placemarks} = initConfig;

  let mapProp = {
    center: new google.maps.LatLng(placemarks[0].lat, placemarks[0].lng),
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: StyleMap,
    scrollwheel: false,
    disableDefaultUI: false
  };

  let map = new google.maps.Map(document.getElementById('contactsMap'), mapProp);

  for (var i = 1; i <= placemarks.length; i++) {

    var data = placemarks[i - 1];
    var myLatlng = new google.maps.LatLng(data.lat, data.lng);

    var block = `<div><img src="${data.icon}" alt=""></div>`;

    var marker = new MarkerWithLabel({
      position: myLatlng,
      map: map,
      title: data.title,
      icon: ' ',
      labelContent: block,
      labelAnchor: new google.maps.Point(28, 63),
      labelClass: 'marker-title',
      labelInBackground: false
    });

    newMarkers.push(marker);

    // initBox(marker, data);

    // var bounds = new google.maps.LatLngBounds();
    //
    // for (var i = 0; i < newMarkers.length; i++) {
    //   bounds.extend(newMarkers[i].getPosition());
    // }
    //
    // map.fitBounds(bounds);

    $(window).on('resize', function() {
      google.maps.event.trigger(map, 'resize');
      map.fitBounds(bounds);
    });

    function clickRoute(lati, long) {

      var pos = {
        position: {
          lat: lati,
          lng: long,
        }
      };
      
      map.panTo(pos.position);
    }

    $('.contactsTabs__list a').on('click', function(e) {
      e.preventDefault();
      let arr = $(this).data('coords').split(' ');
      clickRoute(Number(arr[0]), Number(arr[1]));
    });

  }
};



