import initBox from './initBox';
import {styles as StyleMap} from './style';

export default function locationMap() {

  let MarkerWithLabel = require('./markerwithlabel.js')(google.maps);
  let newMarkers = [];
  let {placemarks} = locationConfig[0];

  let mapProp = {
    center: new google.maps.LatLng(placemarks[0].lat, placemarks[0].lng),
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: StyleMap,
    scrollwheel: false,
    disableDefaultUI: true
  };

  let map = new google.maps.Map(document.getElementById('locationMap'), mapProp);

  for (var i = 1; i <= placemarks.length; i++) {

    var data = placemarks[i - 1];
    var myLatlng = new google.maps.LatLng(data.lat, data.lng);

    var block = `<div><img src="${data.icon}" alt=""></div>`;

    let infowindow = new google.maps.InfoWindow({
      content: data.title
    });

    let marker = new google.maps.Marker({
      position: myLatlng,
      icon: data.icon,
      map: map,
      title: data.title
    });

    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });

    newMarkers.push(marker);

    var bounds = new google.maps.LatLngBounds();

    for (var i = 0; i < newMarkers.length; i++) {
      bounds.extend(newMarkers[i].getPosition());
    }

    var mapZoom = null;

    google.maps.event.addListenerOnce(map, 'bounds_changed', function() {
      if (mapZoom !== map.getZoom()) {
        mapZoom = 12;
        map.setZoom(mapZoom);
      }
    });

    map.fitBounds(bounds);

    $(window).on('resize', function() {
      google.maps.event.trigger(map, 'resize');
      map.fitBounds(bounds);
    });


    google.maps.Map.prototype.clearOverlays = function() {
      for (var i = 0; i < newMarkers.length; i++ ) {
        newMarkers[i].setMap(null);
      }
      newMarkers.length = 0;
    };

  }
};



