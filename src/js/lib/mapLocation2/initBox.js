import InfoBox from './infobox';

export default function initBox(marker, data, map) {
  var div = `<div class="locationBox">
              <div class="locationBox__body">
                <div class="locationBox__title">${data.title}</div>
              </div>
            </div>`;

  var myOptions = {
    content: div,
    disableAutoPan: false,
    maxWidth: 0,
    pixelOffset: new google.maps.Size(-80, 50),
    zIndex: null,
    boxStyle: {
      background: '',
      width: '25rem'
    },
    closeBoxMargin: '10px 10px 2px 2px',
    closeBoxURL: '',
    infoBoxClearance: new google.maps.Size(1, 1),
    isHidden: false,
    pane: 'floatPane',
    enableEventPropagation: false
  };

  marker.infobox = new InfoBox(myOptions);

  google.maps.event.addListener(marker, 'click', function() {

    if ($(window).width() > 900) {
      var curMarker = this;
      marker.infobox.open(map, marker);
      marker.set('labelClass', 'marker-title active');

      $.each(newMarkers, function(index, marker) {

        if (marker !== curMarker) {
          marker.infobox.close();
          marker.set('labelClass', 'marker-title');
        }
      });

      var latUrl = parseFloat(marker.position.toUrlValue().split(',', 2)[0]);
      var lngUrl = parseFloat(marker.position.toUrlValue().split(',', 2)[1]);

      map.panTo({lat: latUrl, lng: lngUrl});
    }

  });
}
