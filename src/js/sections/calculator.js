import $ from 'jquery';

$('.form--calculator').on('submit', function(e) {
  e.preventDefault();


  $.ajax({
    method: 'POST',
    url: $(this).attr('action'),
    data: $(this).serialize(),
    dataType: 'json',
    beforeSend: function() {
      $('.calculator').addClass('is-loading');
    },
    success: function(data) {
      $('.calculatorOffer__info').html(data.info);
      $('.calculatorOffer__preview').css('background-image', `url(${data.previewImage})`);
      $('.calculator').removeClass('is-loading');
      $('.calculatorOffer').addClass('is-active');
      $('.calculator .tabs__con').addClass('is-hidden');
    }
  });

});


$('.form--calculator-square .form__field--type select').on('change', function() {

  $.ajax({
    method: 'POST',
    dataType: 'json',
    data: {
      id: this.value
    },
    url: $('[name="find-type-fields"]').attr('content'),
    beforeSend: function() {
    },
    error: function(xhr) {
      alert(xhr.status + ':' + xhr.statusText);
    },
    success: function({square, floors, price}) {

      let squareFields = `
            <div class="form__field form__field--square">
                  <label class="label">Площадь номера</label>
                  
                   <select name="squareFlat" class="js-select">
                      </select>
              </div>
          `;

      $('.form--calculator-square .form__field--square').replaceWith(squareFields);


      square.forEach((item) => {
        $('.form--calculator-square .form__field--square').find('.js-select').append(`
            <option value=${item.value}>${item.title}</option>
            `);
      });

      $('.form--calculator-square .js-select').selectric();

    }
  });

});

$(document).on('click', '.calculatorOffer__button a', function(e) {
  e.preventDefault();

  $('.calculatorOffer').removeClass('is-active');
  $('.calculator .tabs__con').removeClass('is-hidden');
  $('.form--calculator')[0].reset();
  $('.form--calculatorOffer')[0].reset();
});

$('.calculator .tabsList a').on('click', function() {
  $('.tabs__con').removeClass('is-hidden');
  $('.calculatorOffer').removeClass('is-active');
});
