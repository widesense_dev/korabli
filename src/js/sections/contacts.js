import contactsMap from '../lib/map/index';

if ($('#contactsMap').length) {
  contactsMap();
}

import locationMap from '../lib/mapLocation/index';

if ($('#locationMap').length) {
  locationMap();
}

import locationMap2 from '../lib/mapLocation2/index';

if ($('#locationMap2').length) {
  locationMap2();
}


for (let i = 1; i <= locationConfig.length; i++) {

  let data =  locationConfig[i-1];

  let block;

  if (i === 1) {
    block = `
<li class="locationObjects__item is-active">
                      <a href="#"> ${data.title}
                      </a>
                    </li>
`;
  } else {
    block = `
<li class="locationObjects__item">
                      <a href="#"> ${data.title}
                      </a>
                    </li>
`;
  }


  $('.locationObjects').append(block);

}



$('.contactsTabs__list a').on('click', function() {
  $('.contactsTabs__list a').removeClass('is-active');
  $(this).addClass('is-active');

  var tab_id = $(this).attr('class').split(' ');


  $('.contactsTabs__con').removeClass('is-active');
  $(`.contactsTabs__con_${tab_id[0]}`).addClass('is-active');
});
