import Swiper from 'swiper/dist/js/swiper.js';

import {TimelineMax} from 'gsap';
import $ from 'jquery';

$('.page-column').each(function() {

  let self = $(this).find('.gallery-slider');

  let gallerySlider = new Swiper(self, {
    navigation: {
      nextEl: self.find('.swiper-button-next'),
      prevEl: self.find('.swiper-button-prev'),
    },
    speed: 800,
    preloadImages: false,
    // Enable lazy loading
    lazy: true,
    observer: true,
    observeParents: true,
    grabCursor: false
  });

  let tl = new TimelineMax({
    paused: true
  });

  tl
    .to('.page-column', 0.65, {
      width: '100%',
      height: '100%'
    })
    .add(function() {
      gallerySlider.update();
      gallerySlider.slideTo(0);
    })
    .to('.gallery__close, .gallery__bottom, .gallery-slider .swiper-controls', 0.3, {
      opacity: 1,
      visibility: 'visible'
    });

  $(this).find('.gallery__button').on('click', function() {

    if ($('body').hasClass('is-show-gallery')) {
      tl.reverse();
      $.fn.fullpage.setAllowScrolling(true);
    } else {
      $.fn.fullpage.setAllowScrolling(false);
      tl.play();
    }

    $('body').toggleClass('is-show-gallery');
  });

  $('.gallery__close').on('click', function() {
    tl.reverse();
    $('body').removeClass('is-show-gallery');
  });
});




