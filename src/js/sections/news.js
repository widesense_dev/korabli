import {ACTIVE} from '../_const';
import SimpleBar from 'simplebar';
let count = 0;

const el = new SimpleBar($('.single__content')[0]);

$(document).on('click', '.newsModal-years a', function(e) {
  e.preventDefault();

  let self = $(this);

  $('.newsModal-years a').removeClass('is-active');
  self.addClass('is-active');

  $.ajax({
    method: 'GET',
    dataType: 'json',
    url: $('[name="find-news"]').attr('content'),
    data: {
      year: Number(self.text()),
      counter: count
    },
    success: function({year, newsList, counter, more_news}) {

      if (more_news !== true) {
        $('.newsModal__more').hide();
        count = 0;
      } else {
        $('.newsModal__more').show();
        count = count + counter;
      }

      $('.newsModal__list').html('');

      newsList.forEach((item) => {
        $('.newsModal__list').append(`
            <div class="card card--news-row" data-single="${item.id}">
                <a href="#" class="card__link"></a>
                <div class="card__photo" style="background-image: url(${item.preview_image});"></div>
                <div class="card__body">
                  <div class="card__title">${item.title}</div>
                  <div class="card__date">${item.date}</div>
                  <div class="card__text">${item.description}</div>
                  <div class="card__bottom">
                    <a href="#">
                      <svg class="icon icon-arrow-long-right">
                        <use xlink:href="img/sprite.svg#icon-arrow-long-right"></use>
                      </svg>
                    </a>
                  </div>
                </div>
              </div>        
        `);
      });

    }
  });
});

$(document).on('click', '.newsModal__more', function() {

  $.ajax({
    method: 'GET',
    dataType: 'json',
    url: $('[name="find-news"]').attr('content'),
    data: {
      year: Number($('.newsModal-years a.is-active').text),
      counter: count
    },
    success: function({year, newsList, counter, more_news}) {

      if (more_news !== true) {
        $('.newsModal__more').hide();
        count = 0;
      } else {
        $('.newsModal__more').show();
        count = count + counter;
      }

      newsList.forEach((item) => {
        $('.newsModal__list').append(`
            <div class="card card--news-row" data-single="${item.id}">
                <a href="#" class="card__link"></a>
                <div class="card__photo" style="background-image: url(${item.preview_image});"></div>
                <div class="card__body">
                  <div class="card__title">${item.title}</div>
                  <div class="card__date">${item.date}</div>
                  <div class="card__text">${item.description}</div>
                  <div class="card__bottom">
                    <a href="#">
                      <svg class="icon icon-arrow-long-right">
                        <use xlink:href="img/sprite.svg#icon-arrow-long-right"></use>
                      </svg>
                    </a>
                  </div>
                </div>
              </div>        
        `);
      });

    }
  });
});

$(document).on('click', '[data-single]', function() {
  let card = $(this);

  $.ajax({
    url: $('[name="find-article"]').attr('content'),
    type: 'GET',
    data: {
      id: card.data('single')
    },
    dataType: 'json',
    success: function({id, title, date, body}) {

      let template = `
          <div class="single__title">${title}</div>
          <div class="single__date">${date}</div>
            <article>${body}</article>
      `;

      let $bodySingle = $('.single__body');

      $bodySingle.html('');
      $bodySingle.html(template);

      el.recalculate();

      $('.popup').removeClass(ACTIVE);
      $('#single').addClass(ACTIVE);
      $('body').addClass('no-scroll');
      $.fn.fullpage.setAllowScrolling(false);
    }
  });
});
