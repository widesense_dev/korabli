import Swiper from 'swiper/dist/js/swiper.js';
import {ACTIVE} from '../_const';
import $ from 'jquery';

let flatsSlider = new Swiper('.flatsList .swiper-container', {
  slidesPerView: 4,
  spaceBetween: 16,
  pagination: {
    el: '.flatsList .swiper-pagination',
    clickable: true
  },
  observer: true,
  observeParents: true,
  watchOverflow: true,
  breakpoints: {
    479: {
      slidesPerView: 1
    }
  }
});

export const roomSlider = new Swiper('.previewRoom .swiper-container', {
  init: false,
  pagination: {
    el: '.previewRoom .swiper-pagination',
    clickable: true
  },
  observer: true,
  observeParents: true,
  watchOverflow: true,
  navigation: {
    nextEl: '.previewRoom .swiper-button-next',
    prevEl: '.previewRoom .swiper-button-prev'
  }
});

let $resultBody = $('.resultBox__body');

$(document).on('change', '.form--selection select, .form--selection input', function() {
  let self = $(this);

  if (!self.closest('.form__field').hasClass('form__field--type')) {
    let form = self.closest('.form');
    let formData = form.serialize();

    $.ajax({
      method: 'POST',
      dataType: 'json',
      data: formData,
      url: form.attr('action'),
      beforeSend: function() {
        $resultBody.addClass('is-loading');
      },
      error: function(xhr) {
        $resultBody.removeClass('is-loading');
        alert(xhr.status + ':' + xhr.statusText);
      },
      success: function({resultSelection}) {
        $resultBody.removeClass('is-loading');

        if ($resultBody.find('.simplebar-content').length) {
          $resultBody.find('.simplebar-content').html('');
        } else {
          $resultBody.html('');
        }

        if (resultSelection === null) {

          if ($resultBody.find('.simplebar-content').length) {
            $resultBody.find('.simplebar-content').html('Ничего не найдено.');
          } else {
            $resultBody.html('Ничего не найдено.');
          }

          return false;
        }

        resultSelection.forEach((item) => {


          let template = `
              <div class="resultBox__row" data-apartment="${item.id}">
                      <div class="resultBox__col">${item.floor}</div>
                      <div class="resultBox__col">${item.type}</div>
                      <div class="resultBox__col">${item.square}</div>
                      <div class="resultBox__col">${item.price}</div>
                    </div>
             `;

          if ($resultBody.find('.simplebar-content').length) {
            $resultBody.find('.simplebar-content').append(template);
          } else {
            $resultBody.append(template);
          }
        });
      }
    });
  } else {
    $.ajax({
      method: 'POST',
      dataType: 'json',
      data: {
        id: this.value
      },
      url: $('[name="find-type-fields"]').attr('content'),
      beforeSend: function() {
      },
      error: function(xhr) {
        alert(xhr.status + ':' + xhr.statusText);
      },
      success: function({square, floors, price}) {
        let div = $('<div class="form__area"/>');

        let floorsFields = `
          <div class="form__field">
                        <label class="label">Этажи</label>
                        <div class="floorNumber__list">
                        </div>
                    </div>
          `;

        let squareFields = `
            <div class="form__field form__field--square">
                  <label class="label">Площадь</label>

                  <div class="form__selects">
                      <select name="squareStart" class="js-select">
                      </select>
                      <span>до</span>
                      <select name="squareEnd" class="js-select">
                      </select>
                  </div>
              </div>
          `;

        let priceFields = `
            <div class="form__field form__field--price">
                <label class="label">Стоимость, млн</label>

                <div class="form__selects">
                    <select name="priceStart" class="js-select">
                    </select>
                    <span>до</span>
                    <select name="priceEnd" class="js-select">
                    </select>
                </div>
            </div>          
          `;

        div.append(floorsFields);
        div.append(squareFields);
        div.append(priceFields);

        $('.form--selection .form__area').replaceWith(div);

        floors.forEach((item) => {

          $('.form--selection .form__area').find('.floorNumber__list').append(`
                <div class="floorNumber">
                    <input type="radio" name="floorNumber" value="${item.value}" />
                    <span>${item.title}</span>
                </div>
            `);
        });

        square.forEach((item, index) => {

          $('.form--selection .form__field--square').find('.js-select').append(`
            <option value=${item.value}>${item.title}</option>
            `);

        });

        price.forEach((item) => {
          $('.form--selection .form__field--price').find('.js-select').append(`
            <option value=${item.value}>${item.title}</option>
            `);
        });

        $('.form--selection .form__field--square .js-select:last-child option:last').attr('selected', 'selected');
        $('.form--selection .form__field--price .js-select:last-child option:last').attr('selected', 'selected');

        $('.form--selection .js-select')
          .selectric();

        let form = self.closest('.form');
        let formData = form.serialize();

        $.ajax({
          method: 'POST',
          dataType: 'json',
          data: formData,
          url: form.attr('action'),
          beforeSend: function() {
            $resultBody.addClass('is-loading');
          },
          error: function(xhr) {
            $resultBody.removeClass('is-loading');
            alert(xhr.status + ':' + xhr.statusText);
          },
          success: function({resultSelection}) {
            $resultBody.removeClass('is-loading');

            if ($resultBody.find('.simplebar-content').length) {
              $resultBody.find('.simplebar-content').html('');
            } else {
              $resultBody.html('');
            }

            if (resultSelection === null) {

              if ($resultBody.find('.simplebar-content').length) {
                $resultBody.find('.simplebar-content').html('Ничего не найдено.');
              } else {
                $resultBody.html('Ничего не найдено.');
              }

              return false;
            }

            resultSelection.forEach((item) => {


              let template = `
              <div class="resultBox__row" data-apartment="${item.id}">
                      <div class="resultBox__col">${item.floor}</div>
                      <div class="resultBox__col">${item.type}</div>
                      <div class="resultBox__col">${item.square}</div>
                      <div class="resultBox__col">${item.price}</div>
                    </div>
             `;

              if ($resultBody.find('.simplebar-content').length) {
                $resultBody.find('.simplebar-content').append(template);
              } else {
                $resultBody.append(template);
              }
            });
          }
        });

      }
    });
  }
});

$(document).on('click', '[data-apartment]', function(e) {
  e.preventDefault();

  let url = $('[name="find-apartment"]').attr('content');

  if ($(this).closest('.popup').attr('id') === 'selectionNumber') {
    window.navWatch = 'selectionNumber';
  } else {
    window.navWatch = '';
  }

  if ($(window).width() > 479) {

    $('.popup').removeClass(ACTIVE);
    $('#roomCard').addClass(ACTIVE).addClass('is-loading');

  } else {
    $.fancybox.open({
      src: '#roomCard',
      type: 'inline',
      opts: {
        afterShow: function(instance, current) {
          console.info('done!');
        }
      }
    });
  }

  $('body').addClass('no-scroll');
  $.fn.fullpage.setAllowScrolling(false);

  let idRoom = $(this).data('apartment');

  $.ajax({
    method: 'POST',
    dataType: 'json',
    url: url,
    data: {
      id: idRoom
    },
    success: function(data) {

      window.history.pushState({}, '', window.location.origin + '/');

      window.history.pushState({}, '', window.location.href + '?' + $.param({roomId: idRoom}));

      if (data === null) {
        alert('Сервер прислал NULL');

        $('.popup').removeClass(ACTIVE);
        $('#objectRooms').addClass(ACTIVE);
        $('#roomCard').removeClass('is-loading');
        $('body').addClass('no-scroll');
        $.fn.fullpage.setAllowScrolling(false);

        return false;
      }


      $('#roomCard .roomCard__title').text(`${data.title}`);
      $('#roomCard .roomCard__square').text(`${data.square}`);
      $('#roomCard .roomCard__floor').text(`${data.floor}`);
      $('#roomCard .roomCard__text').text(`${data.description}`);
      $('#roomCard .priceBox__value').text(`${data.price}`);
      $('#roomCard .previewRoom .swiper-wrapper').html('');

      $('#roomCard .profitBox__body').html(data.profit);

      data.images.forEach((item) => {
        let templateSlide = `
          <div class="swiper-slide">
            <div class="previewRoom__photo" style="background-image: url(${item});"></div>
          </div>
      `;
        $('#roomCard .previewRoom .swiper-wrapper').append(templateSlide);

        roomSlider.init();

        $('#roomCard').removeClass('is-loading');
      });
    }
  });

});

$('.btn-back').on('click', function() {
  $('.popup').removeClass(ACTIVE);

  if (window.navWatch === '') {
    $('#objectRooms').addClass('is-active');
  } else {
    $('#selectionNumber').addClass('is-active');
  }
});

$('[data-popup="#canvasObject"]').on('click', function() {

  if ($(window).width() > 479) {

    if (!$('.canvasObject__body svg').length) {
      $.ajax({
        method: 'GET',
        url: $('[data-canvas-url]').data('canvas-url'),
        dataType: 'html',
        success: function(data) {
          $('.canvasObject__body').append(data);
        }
      });
    }

  } else {

  }

})
;

$(document).on('click', '[data-canvas-floor]', function(e) {
  e.preventDefault();

  $(`.floorNumber input[value=${$(this).data('canvas-floor')}]`).prop('checked', 'checked');

  $.ajax({
    method: 'GET',
    url: $('[name="find-floor"]').attr('content'),
    dataType: 'json',
    data: {
      idFloor: $(this).data('canvas-floor')
    },
    beforeSend: function() {
      $('#canvasObject').addClass('is-loading');
    },
    success: function({id, schemeImage, schemeSvg, disabledApartaments}) {
      let schemeBlock = $('.objectRooms__scheme');

      schemeBlock.css('background-image', `url(${schemeImage})`);
      schemeBlock.html('');
      schemeBlock.append(schemeSvg);

      $('#canvasObject').removeClass('is-loading');

      disabledApartaments.forEach((item) => {
        $(`[data-apartment=${item}]`).addClass('is-disabled');
      });

      $('.popup').removeClass(ACTIVE);
      $('#objectRooms').addClass(ACTIVE);
      $('body').addClass('no-scroll');
      $.fn.fullpage.setAllowScrolling(false);
    }
  });

});

$('.form--floors [type="radio"]').on('change', function() {

  $.ajax({
    method: 'GET',
    url: $('[name="find-floor"]').attr('content'),
    dataType: 'json',
    data: {
      idFloor: $(this).val()
    },
    beforeSend: function() {
      $('#canvasObject').addClass('is-loading');
    },
    success: function({id, schemeImage, schemeSvg, disabledApartaments}) {
      let schemeBlock = $('.objectRooms__scheme');

      schemeBlock.css('background-image', `url(${schemeImage})`);
      schemeBlock.html('');
      schemeBlock.append(schemeSvg);

      $('#canvasObject').removeClass('is-loading');

      disabledApartaments.forEach((item) => {
        $(`[data-apartment=${item}]`).addClass('is-disabled');
      });

      $('.popup').removeClass(ACTIVE);
      $('#objectRooms').addClass(ACTIVE);
      $('body').addClass('no-scroll');
      $.fn.fullpage.setAllowScrolling(false);
    }
  });
});

$('[data-type-id]').on('click', function(e) {
  e.preventDefault();

  $('.form__field--type select').prop('selectedIndex', Number($(this).data('type-id')) - 1).selectric('refresh');

  $.ajax({
    method: 'POST',
    dataType: 'json',
    data: {
      id: $(this).data('type-id')
    },
    url: $('[name="find-type-fields"]').attr('content'),
    beforeSend: function() {
    },
    error: function(xhr) {
      alert(xhr.status + ':' + xhr.statusText);
    },
    success: function({square, floors, price}) {
      let div = $('<div class="form__area"/>');

      let floorsFields = `
          <div class="form__field">
                        <label class="label">Этажи</label>
                        <div class="floorNumber__list">
                        </div>
                    </div>
          `;

      let squareFields = `
            <div class="form__field form__field--square">
                  <label class="label">Площадь</label>

                  <div class="form__selects">
                      <select name="squareStart" class="js-select">
                      </select>
                      <span>до</span>
                      <select name="squareEnd" class="js-select">
                      </select>
                  </div>
              </div>
          `;

      let priceFields = `
            <div class="form__field form__field--price">
                <label class="label">Стоимость, млн</label>

                <div class="form__selects">
                    <select name="priceStart" class="js-select">
                    </select>
                    <span>до</span>
                    <select name="priceEnd" class="js-select">
                    </select>
                </div>
            </div>          
          `;

      div.append(floorsFields);
      div.append(squareFields);
      div.append(priceFields);

      $('.form--selection .form__area').replaceWith(div);

      floors.forEach((item) => {

        $('.form--selection .form__area').find('.floorNumber__list').append(`
                <div class="floorNumber">
                    <input type="radio" name="floorNumber" value="${item.value}" />
                    <span>${item.title}</span>
                </div>
            `);
      });

      square.forEach((item, index) => {

        $('.form--selection .form__field--square').find('.js-select').append(`
            <option value=${item.value}>${item.title}</option>
            `);

      });

      price.forEach((item) => {
        $('.form--selection .form__field--price').find('.js-select').append(`
            <option value=${item.value}>${item.title}</option>
            `);
      });

      $('.form--selection .form__field--square .js-select:last-child option:last').attr('selected', 'selected');
      $('.form--selection .form__field--price .js-select:last-child option:last').attr('selected', 'selected');

      $('.form--selection .js-select')
        .selectric();

      let form = $('.form--selection');
      let formData = form.serialize();

      $.ajax({
        method: 'POST',
        dataType: 'json',
        data: formData,
        url: form.attr('action'),
        beforeSend: function() {
          $resultBody.addClass('is-loading');
        },
        error: function(xhr) {
          $resultBody.removeClass('is-loading');
          alert(xhr.status + ':' + xhr.statusText);
        },
        success: function({resultSelection}) {
          $resultBody.removeClass('is-loading');

          if ($resultBody.find('.simplebar-content').length) {
            $resultBody.find('.simplebar-content').html('');
          } else {
            $resultBody.html('');
          }

          if (resultSelection === null) {

            if ($resultBody.find('.simplebar-content').length) {
              $resultBody.find('.simplebar-content').html('Ничего не найдено.');
            } else {
              $resultBody.html('Ничего не найдено.');
            }

            return false;
          }

          resultSelection.forEach((item) => {


            let template = `
              <div class="resultBox__row" data-apartment="${item.id}">
                      <div class="resultBox__col">${item.floor}</div>
                      <div class="resultBox__col">${item.type}</div>
                      <div class="resultBox__col">${item.square}</div>
                      <div class="resultBox__col">${item.price}</div>
                    </div>
             `;

            if ($resultBody.find('.simplebar-content').length) {
              $resultBody.find('.simplebar-content').append(template);
            } else {
              $resultBody.append(template);
            }
          });
        }
      });

    }
  });
});
