import Swiper from 'swiper/dist/js/swiper.js';
import {ACTIVE} from '../_const';
import $ from 'jquery';

const sliderProgress = new Swiper('.newsBox__slider .swiper-container', {
  slidesPerView: 1,
  observer: true,
  observeParents: true,
  watchOverflow: true,
  navigation: {
    nextEl: '.newsBox__slider .swiper-button-next',
    prevEl: '.newsBox__slider .swiper-button-prev',
  },
  pagination: {
    clickable: true,
    el: '.newsBox__slider .swiper-pagination'
  },
  on: {
    init: function() {
      setTimeout(function() {
        let $el = $('.stagesMonths__item a.is-active');
        $el.text();

        $('.newsBox .swiper-slide').not('[data-month="' + $el.text() + '"]').addClass('non-swiper-slide').removeClass('swiper-slide').hide();
        $('[data-month=' + $el.text() + ']').removeClass('non-swiper-slide').addClass('swiper-slide').attr('style', null).show();
        sliderProgress.update();
      }, 200);
    }
  }
});

let sliderBody = $('.newsBox__slider .swiper-wrapper');
let currMonth = '';

$(document).on('click', '.stagesMonths__item a', function(e) {
  e.preventDefault();
  let self = $(this);
  $('.stagesMonths__item a').removeClass(ACTIVE);

  self.addClass(ACTIVE);
  $('.newsBox .swiper-slide').not('[data-month="' + self.text() + '"]').addClass('non-swiper-slide').removeClass('swiper-slide').hide();
  $('[data-month=' + self.text() + ']').removeClass('non-swiper-slide').addClass('swiper-slide').attr('style', null).show();
  sliderProgress.update();
  sliderProgress.slideTo(0);
});

$(document).on('click', '.stagesNav--rounds a', function(e) {
  e.preventDefault();
  let self = $(this);

  $.ajax({
    method: 'GET',
    dataType: 'json',
    url: self.attr('href'),
    data: {
      'round_id': self.data('round')
    },
    success: function({years, months, news}) {

      $('.stagesNav--rounds a').removeClass(ACTIVE);
      self.addClass(ACTIVE);

      $('.stagesNav--years').html('');
      $('.stagesMonths').html('');
      sliderBody.html('');

      years.forEach((item) => {
        $('.stagesNav--years').append(`
                <li class="stagesNav__item">
                    <a href="#" ${item.active ? 'class="is-active"' : ''}>${item.title}</a>
                </li>
        `);
      });

      months.listMonths.forEach((item) => {
        item.current ? currMonth = item.title : '';

        $('.stagesMonths').append(`
          <li class="stagesMonths__item">
            <a href="#" ${item.disabled ? 'class="is-disabled"' : ''} ${item.current ? 'class="is-active"' : ''}>${item.title}</a>
          </li>
        `);
      });

      news.newsList.forEach((item) => {
        sliderBody.append(`
            <div data-month="${item.month}" class="swiper-slide" style="width: 252px;">
                <div class="card card--news" data-single="${item.id}">
                  <a href="#" class="card__link" data-popup="#single"></a>
                  <div class="card__photo" style="background-image: url(${item.preview_image});"></div>
                  <div class="card__body">
                    <div class="card__title">${item.title}</div>
                    <div class="card__date">${item.date}</div>
                    <div class="card__text">${item.description}</div>
                    <div class="card__bottom">
                      <a href="#">
                        <svg class="icon icon-arrow-long-right">
                          <use xlink:href="img/sprite.svg#icon-arrow-long-right"></use>
                        </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
        `);
      });

      $('.newsBox .swiper-slide').not('[data-month="' + currMonth + '"]').addClass('non-swiper-slide').removeClass('swiper-slide').hide();
      $('[data-month=' + currMonth + ']').removeClass('non-swiper-slide').addClass('swiper-slide').attr('style', null).show();
      sliderProgress.update();
    }
  });

});

$(document).on('click', '.stagesNav--years a', function(e) {
  e.preventDefault();
  let self = $(this);

  $.ajax({
    method: 'POST',
    url: $('[name="find-months"]').attr('content'),
    data: {
      round_id: $('.stagesNav--rounds a.is-active').data('round'),
      year: Number(self.text())
    },
    success: function({months, news}) {

      $('.stagesNav--years a').removeClass(ACTIVE);
      self.addClass(ACTIVE);

      $('.stagesMonths').html('');
      sliderBody.html('');

      months.listMonths.forEach((item) => {
        item.current ? currMonth = item.title : '';

        $('.stagesMonths').append(`
          <li class="stagesMonths__item">
            <a href="#" ${item.disabled ? 'class="is-disabled"' : ''} ${item.current ? 'class="is-active"' : ''}>${item.title}</a>
          </li>
        `);
      });

      news.newsList.forEach((item) => {
        sliderBody.append(`
            <div data-month="${item.month}" class="swiper-slide" style="width: 252px;">
                <div class="card card--news" data-single="${item.id}">
                  <a href="#" class="card__link" data-popup="#single"></a>
                  <div class="card__photo" style="background-image: url(${item.preview_image});"></div>
                  <div class="card__body">
                    <div class="card__title">${item.title}</div>
                    <div class="card__date">${item.date}</div>
                    <div class="card__text">${item.description}</div>
                    <div class="card__bottom">
                      <a href="#">
                        <svg class="icon icon-arrow-long-right">
                          <use xlink:href="img/sprite.svg#icon-arrow-long-right"></use>
                        </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
        `);
      });

      $('.newsBox .swiper-slide').not('[data-month="' + currMonth + '"]').addClass('non-swiper-slide').removeClass('swiper-slide').hide();
      $('[data-month=' + currMonth + ']').removeClass('non-swiper-slide').addClass('swiper-slide').attr('style', null).show();
      sliderProgress.update();
    }
  });


});

$('.stagesMonths__item a.is-active').parent().addClass('is-showing');
$('.stagesMonths__item a.is-active').parent().next().addClass('is-showing');
$('.stagesMonths__item a.is-active').parent().prev().addClass('is-showing');

$('.stages__body').append(`
<div class="stages__bottom">
<a href="javascript:void(0);" class="js-toggle-months">
<svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M1.41387 0.000187954L3.71054e-05 1.41403L8.48513 9.89949L9.89934 8.48528L1.41387 0.000187954Z" fill="white"></path>
<path d="M8.48438 9.89941L7.07016 8.4852L15.5556 3.13216e-05L16.9697 1.41413L8.48438 9.89941Z" fill="white"></path>
</svg>
</a>
</div> 
`);

$(document).on('click', '.js-toggle-months', function(e) {
  e.preventDefault();

  $('.stagesMonths').toggleClass(ACTIVE);
  $(this).toggleClass(ACTIVE);
});
